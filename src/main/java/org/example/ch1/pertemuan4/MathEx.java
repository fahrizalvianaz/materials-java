package org.example.ch1.pertemuan4;

import java.util.Objects;

public class MathEx {

    public static void main(String[] args) {
        System.out.println(Math.max(10,11));
        System.out.println(Math.min(10,11));


//        Equal

        String value= "123";
        System.out.println(value.equals("12tiga"));
        System.out.println(value.equals("123"));
        System.out.println("===================");
        Integer val = 123;
        System.out.println(val.equals(123));
        System.out.println(val.equals(12345));
        System.out.println("===================");
        System.out.println(Objects.equals("123","123"));
        System.out.println(Objects.equals(123,456));

    }


}
