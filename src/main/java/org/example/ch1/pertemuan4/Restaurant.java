package org.example.ch1.pertemuan4;

import java.util.Scanner;

public class Restaurant {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean inginPesanLagi = true;
        double totalHarga = 0;

        System.out.println("Selamat datang di Restoran Team C!");

        do {
            System.out.println("\nMenu Makanan:");
            System.out.println("1. Nasi Goreng  | Rp. 15.000 ");
            System.out.println("2. Mie Goreng   | Rp. 10.000 ");
            System.out.println("3. Ayam Goreng  | Rp. 20.000 ");
            System.out.print("Pilih Menu : ");
            int pilih = input.nextInt();

            int harga;
            String namaMenu = "";
            switch (pilih) {
                case 1:
                    harga = 15000;
                    namaMenu = "Nasi Goreng";
                    break;
                case 2:
                    harga = 10000;
                    namaMenu = "Mie Goreng";
                    break;
                case 3:
                    harga = 20000;
                    namaMenu = "Ayam Goreng";
                    break;
                default:
                    harga = 0;
                    System.out.println("Menu tidak tersedia");
                    continue;
            }

            System.out.print("Jumlah pesanan " + namaMenu + ": ");
            int qty = input.nextInt();

            totalHarga += harga * qty;

            System.out.print("Apakah Anda ingin memesan lagi? (ya/tidak): ");
            String pesanLagi = input.next();
            if (!pesanLagi.equalsIgnoreCase("ya")) {
                inginPesanLagi = false;
            }
        } while (inginPesanLagi);

        System.out.println("Terima kasih telah memesan!");
        System.out.println("Total belanja Anda adalah: Rp. " + totalHarga);
        input.close();
    }
}