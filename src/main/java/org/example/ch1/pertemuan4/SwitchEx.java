package org.example.ch1.pertemuan4;

public class SwitchEx {

    public static void main(String[] args) {

        String grade = "A";
        String hasil = null;

        switch(grade) {
            case "A":
                hasil = "Excellent!";
                break;
            case "B":
                hasil = "Good job!";
                break;
            case "C":
                hasil = "Well done";
                break;
            case "D":
                hasil = "You passed";
                break;
            case "F":
                hasil = "Better try again";
                break;
            default:
                hasil = "Invalid grade";
        }

        String hasilSwitch = switch (grade) {
            case "A" -> "Excellent!";
            case "B" -> "Good job!";
            case "C" -> "Well done";
            case "D" -> "You passed";
            case "F" -> "Better try again";
            default -> "Invalid grade";
        };
        System.out.println("Your grade is " + grade);
        System.out.println("Your result is " + hasilSwitch);
    }


}
