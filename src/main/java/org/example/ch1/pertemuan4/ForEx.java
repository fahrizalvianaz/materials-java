package org.example.ch1.pertemuan4;

public class ForEx {

    public static void main(String[] args) {
        manual();
        forLoop();
    }

    public static void forLoop() {
        for (int i = 0; i < 5; i++) {
            System.out.println("Hello World");
        }
    }

    public static void manual () {
        System.out.println("Cetak Saya");
        System.out.println("Cetak Saya");
        System.out.println("Cetak Saya");
        System.out.println("Cetak Saya");
        System.out.println("Cetak Saya");

    }




}
