package org.example.ch1.pertemuan4;

public class IfElseEx {

    public static void main(String[] args) {

        int a = 30;
        int b = 30;
        int c = 30;

        if(a == b) {
            System.out.println("a sama dengan b");
        } else if(a > b) {
            System.out.println("a lebih besar dari b");
        } else {
            System.out.println("a lebih kecil dari b");
        }

        checkAngka(a, b, c);

    }
    public static void checkAngka(int a, int b, int c) {
        if(a == b || b == c) {
            System.out.println("equals");
        } else if(a > b) {
            System.out.println("a lebih besar dari b");
        } else {
            System.out.println("a lebih kecil dari b");
        }
    }

}
