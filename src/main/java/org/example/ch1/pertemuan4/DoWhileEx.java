package org.example.ch1.pertemuan4;

public class DoWhileEx {

    public static void main(String[] args) {

        int var = 23;
        do {
            System.out.println("value " + var);
            var--;
        } while (var > 20);

    }


}
