package org.example.ch1.pertemuan6;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.StringJoiner;

public class DateGroup {

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("current data : " + date);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMMM-dd HH:mm:ss");
        System.out.println("current data : " + sdf.format(date));

    }
}
