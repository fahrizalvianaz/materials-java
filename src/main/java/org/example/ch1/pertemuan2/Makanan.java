package org.example.ch1.pertemuan2;

import java.util.Scanner;

public class Makanan {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Selamat datang di Warmindo C!");
        System.out.println("Silahkan pilih menu makanan yang diinginkan :");
        System.out.println("Nasi Goreng  | Rp. 15.000 ");
        System.out.println("Mie Goreng   | Rp. 10.000 ");
        System.out.println("Ayam Goreng  | Rp. 20.000 ");
        System.out.print("Pilih Menu :");
        int pilih = input.nextInt();
        System.out.print("Jumlah pesananan: ");
        int qty = input.nextInt();

        input.close();

        System.out.println("\n--------------------\n");


        order(pilih, qty);

    }
    public static void order(int pilih, int qty) {
        double totalHarga = 0.0;
        System.out.println("Detail Pesanan : ");
        if( pilih == 1) {
            System.out.println("Nasi Goreng -> 15.000");
            System.out.println("Jumlah pesanan : " + qty);
            totalHarga = 15000 * qty;
            System.out.println("Total Harga: " + totalHarga);
        } else if (pilih == 2) {
            System.out.println("Mie Goreng -> 10.000");
            System.out.println("Qty : " + qty);
            totalHarga = 10000 * qty;
            System.out.println("Total Harga: " + totalHarga);
        } else if (pilih == 3) {
            System.out.println("Ayam Goreng -> 20.000");
            System.out.println("Qty : " + qty);
            totalHarga = 20000 * qty;
            System.out.println("Total Harga: " + totalHarga);
        } else {
            System.out.println("Menu tidak tersedia");
        }

    }

}
