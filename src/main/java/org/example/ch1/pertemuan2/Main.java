package org.example.ch1.pertemuan2;


import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter an integer : ");
        String str = input.nextLine();
        System.out.println("You entered : " + str);

        input.close();
    }


}