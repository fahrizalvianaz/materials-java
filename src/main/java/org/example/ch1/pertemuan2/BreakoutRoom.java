package org.example.ch1.pertemuan2;

import java.util.Scanner;

public class BreakoutRoom {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        double totalHarga = 0;
        System.out.println("Nasi Goreng  | Rp. 15.000 ");
        System.out.println("Mie Goreng   | Rp. 10.000 ");
        System.out.println("Ayam Goreng  | Rp. 20.000 ");


        System.out.print("Pilih menu : ");
        int pilih = input.nextInt();
        System.out.print("Jumlah : ");
        int qty = input.nextInt();
        input.close();
        System.out.println("\n--------------------\n");

        switch (pilih) {
            case 1:
                System.out.println("Nasi Goreng -> 15.000");
                System.out.println("Qty : " + qty);
                totalHarga = 15000 * qty;
                System.out.println("Total : " + totalHarga);
                break;
            case 2:
                System.out.println("Mie Goreng -> 10.000");
                System.out.println("Qty : " + qty);
                totalHarga = 10000 * qty;
                System.out.println("Total : " + totalHarga);
                break;
            case 3:
                System.out.println("Magelangan -> 11.000");
                System.out.println("Qty : " + qty);
                totalHarga = 11000 * qty;
                System.out.println("Total : " + totalHarga);
                break;
            default:
                System.out.println("Menu tidak tersedia");
                break;
        }


    }
}
