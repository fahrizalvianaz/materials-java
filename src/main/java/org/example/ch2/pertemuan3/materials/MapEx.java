package org.example.ch2.pertemuan3.materials;

import java.util.HashMap;
import java.util.Map;

public class MapEx {
    public static void main(String[] args) {
        // Membuat objek map
        Map<String, String> map = new HashMap();
        map.put("name", "John Doe");
        map.put("email", "fahri@gmail.com");
        map.put("phone", "081234567898:");

        System.out.println(map.get("name"));
    }
}
