package org.example.ch2.pertemuan3.materials;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListArrayEx {
    public static void main(String[] args) {
        List<String> objList = new ArrayList<>();
        // nambahin value ?
        objList.add("nasi goreng");
        objList.add("ayam goreng");
        objList.add("nasi goreng");
        objList.add("nasi goreng");
        objList.add("nasi goreng");

        //cetak result
        for(String  obj : objList){
            System.out.println(obj);
        }
    }
}
