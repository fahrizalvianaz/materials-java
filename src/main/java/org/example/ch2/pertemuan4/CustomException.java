package org.example.ch2.pertemuan4;

import java.io.IOException;

class CustomException extends IOException {
    public CustomException(String message) {
        super(message);
    }
}