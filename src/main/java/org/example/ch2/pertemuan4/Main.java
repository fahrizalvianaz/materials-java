package org.example.ch2.pertemuan4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        boolean r=true;
        double t=0,h=0;
        Scanner input=new Scanner(System.in);
        System.out.println("==========================");
        System.out.println("Selamat datang di Binarfud");
        System.out.println("==========================\n");
        //iniliasasi map utk (menampilkan struk pembayaran), memperbolehkan null
        Map<String, Integer> map = new HashMap<>();
        while (r){
            System.out.println("Silahkan pilih menu :");
            System.out.println("1. Nasi Goreng   |  15.000");
            System.out.println("2. Mie Goreng    |  13.000");
            System.out.println("3. Nasi + Ayam   |  18.000");
            System.out.println("4. Es Teh Manis  |   3.000");
            System.out.println("5. Es Jeruk      |   5.000");
            System.out.println("Pilih menu :");
            int i=input.nextInt();
            String temp="";
            switch (i) {
                case 1:
                    h = 15000;
                    temp="Nasi Goreng";
                    break;
                case 2:
                    h = 13000;
                    temp="Mie Goreng";
                    break;
                case 3:
                    h = 18000;
                    temp="Nasi + Ayam";
                    break;
                case 4:
                    h = 3000;
                    temp="Es Teh Manis";
                    break;
                case 5:
                    h = 5000;
                    temp="Es Jeruk";
                    break;
                default:
                    System.out.println("Menu tidak tersedia");
                    continue;
            }

            System.out.println("Masukkan jumlah :");
            int q=input.nextInt();
            if (map.containsKey(temp)) {
                int existingQuantity = map.get(temp);
                System.out.println("existingQuantity: " + map.get(temp));
                map.put(temp, existingQuantity + q);
            } else {
                map.put(temp, q);
            }
            t+=h*q;
            System.out.println("Apakah anda ingin memesan lagi?(y/n)");
            String pesan= input.next();
            if(!pesan.equalsIgnoreCase("y")){
                r=false;
            }
        }

        System.out.println("========================================");
        System.out.println("Terima kasih telah memesan di Binarfund!\n");
        System.out.println("Berikut adalah rincian pesanan anda :\n");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.printf("%-32s x%-5d%n", entry.getKey(), entry.getValue());
        }
        System.out.println("------------------------------------+");
        System.out.printf("%-28s Rp%-18.0f%n", "Total:", t,"\n");
        System.out.println("\n========================================");
        System.out.println("Simpan struk sebagai bukti transaksi");
        System.out.println("Have a great day ^_^");
        System.out.println("========================================");


        try (BufferedWriter writer = new BufferedWriter(new FileWriter("java/struk.txt"))) {
            writer.write("========================================\n");
            writer.write("Terima kasih telah memesan di Binarfund!\n");
            writer.write("Berikut adalah rincian pesanan anda :\n\n");
            for (Map.Entry<String, Integer> entry : map.entrySet()) {
                writer.write(String.format("%-32s x%-5d%n", entry.getKey(), entry.getValue()));
            }
            writer.write("------------------------------------+\n");
            writer.write(String.format("%-28s Rp%-18.0f%n", "Total:", t));
            writer.write("\n========================================\n");
            writer.write("Simpan struk sebagai bukti transaksi\n");
            writer.write("Have a great day ^_^\n");
            writer.write("========================================\n");

        } catch (IOException e) {
            throw new CustomException("Gagal membuat file struk karena " + e.getMessage());
        }
    }


}