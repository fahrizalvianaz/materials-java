package org.example.ch2.pertemuan4;

public class ExceptionEx {
    public static void main(String[] args) {

        String str = null;
        try {

            int length = str.length();
            System.out.println(length);
        }catch (Exception e){
            System.out.println("handling eror 1="+e.getMessage());
            str = "";

        } finally {
            System.out.println("koneksi ditutup");
        }

        try {
            int[] numbers = {1, 2, 3};
            System.out.println(numbers[3]);
        }catch (Exception e){
            System.out.println("handling eror 1="+e.getMessage());

        }
        try {
            int data = 50 / 0;
            System.out.println(data);
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }

    }
}
