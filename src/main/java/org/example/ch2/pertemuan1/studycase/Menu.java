package org.example.ch2.pertemuan1.studycase;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Menu {

    private String menu;
    private int harga;
    private int qty;


}
