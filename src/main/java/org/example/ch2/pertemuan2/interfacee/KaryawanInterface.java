package org.example.ch2.pertemuan2.interfacee;

public interface KaryawanInterface  {
    // class superclass : induk

    public  void save(String nama);

    public  void update(String nama);

    public  void list(String nama);
}
