
package org.example.ch2.pertemuan2.dto;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class KaryawanDTO {

    Integer id;
    String karyawanName;
}

