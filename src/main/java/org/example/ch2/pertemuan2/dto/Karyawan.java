package org.example.ch2.pertemuan2.dto;

import lombok.Data;

@Data
public class Karyawan {
    Integer id;

    String name;
}
