package org.example.ch3.pertemuan4;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodOrder {

    private String name;
    private double price;
    private int quantity;



}
