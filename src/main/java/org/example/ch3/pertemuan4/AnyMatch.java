package org.example.ch3.pertemuan4;

import java.util.Arrays;
import java.util.List;

public class AnyMatch {
    public static void main(String[] args) {


//        AnyMatch
        List<Integer> numbers1 = Arrays.asList(1, 3, 5, 7, 8, 9);

        boolean hasEven = numbers1.stream().anyMatch(n -> n % 2 == 0);

        System.out.println("Apakah ada bilangan genap? " + hasEven);

//        AllMatch
        List<Integer> numbers2 = Arrays.asList(1, 3, 5, 7, 8, 9);

        boolean allPositive = numbers2.stream().allMatch(n -> n > 0);

        System.out.println("Apakah semua bilangan positif? " + allPositive);
    }
}
