package org.example.ch3.pertemuan3;

import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class LambdaEx {
    public static void main(String[] args) {
        Runnable runnable = () -> {
            System.out.println("Hello world");
        };

        Optional<String> optional = Optional.ofNullable(null);
        String value = optional.orElseGet(() -> "Default value");

//        Supplier
        Supplier<Integer> integerLambda = () -> 1;
        System.out.println(integerLambda.get());

//        Consumer == void
        Consumer<String> myName = (myname)-> {
            System.out.println("my name = "+myname);
        };
//        call consumer
        myName.accept("Fahri");

//        3. Lamda 2 parameter
        BiConsumer<Integer, Integer> sample1 = (x, y) -> System.out.println("nilai x + y = "+(x+y));
        BiConsumer<Integer, Integer> sample2 = (x,y)->{
            System.out.println("nilai ke 2 x + y = "+(x+y));
        };

        sample1.accept(1,1);
        sample2.accept(1,2);
    }
}
