package org.example.ch3.pertemuan2;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Optional;

@Data
@AllArgsConstructor
public class CategoryType {
    private String name;
    private Optional<String> categoryType;
}