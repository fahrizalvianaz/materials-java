package org.example.ch3.pertemuan2;

import java.util.Optional;

public class OptionalClass {
    public static void main(String[] args) {
        Category category = new Category("Electronics");
        // Membuat objek detail produk dengan kategori
        DetailProduct detailProduct = new DetailProduct("Smartphone", Optional.of(category));
        // Membuat objek produk dengan detail produk
        Product product = new Product("iPhone", Optional.of(detailProduct));

        // Mendapatkan nama produk
        String productName = product.getName();
        System.out.println("Product Name: " + productName);

        // Mendapatkan deskripsi produk
        String productDescription = product.getDetailProduct()
                .map(DetailProduct::getDescription)
                .orElse("No description available");
        System.out.println("Product Description: " + productDescription);

        // Mendapatkan nama kategori (jika tersedia)
        String categoryName = product.getDetailProduct()
                .flatMap(DetailProduct::getCategory)
                .map(Category::getName)
                .orElse("No category available");
        System.out.println("Category Name: " + categoryName);

        Optional<String> optionalValue = Optional.ofNullable("Hello");
        String value = optionalValue.orElse("Default Value");
        System.out.println("Value: " + value);

        Optional<DetailProduct> detailProduct1 = Optional.of(detailProduct);
        DetailProduct detailProductNew = new DetailProduct("Smartphone", Optional.of(new Category("Electronics")));
        Optional<String> optionalValue2 = Optional.ofNullable(null);
        String value2 = optionalValue.orElseGet(() -> "Default Value");
        System.out.println("Value2: " + value2);

        Optional<String> optionalValueGet = Optional.ofNullable(null);
        String value3 = optionalValue.orElseGet(() -> "Default Value");
        System.out.println("or else get: "+value3);

        //orelseGet versi 2
        String value4 = optionalValue.orElseGet(() ->{
            //tulis logic yang dibutuhkan
            return "Default Value dari value 4";
        });
        System.out.println("or else get value4 : "+value4);

        //orelseGet Object 3 tampa logic
        DetailProduct value5 = detailProduct1.orElseGet(()->detailProductNew);
        System.out.println("or else get object: "+value5);

        //orelseGet Object dengan logic
        DetailProduct value6 = detailProduct1.orElseGet(()->{
            // menuliskan logic
            return detailProductNew;
        });
        System.out.println("or else get object 2: "+value6);

//        GET
        Optional<String> optionalValueGet1 = Optional.ofNullable("Hello");
        String valueGet1 = optionalValueGet1.get();
        System.out.println("Value Get 1: "+valueGet1);

//        GET Object
        Optional<DetailProduct> detailProductGet = Optional.of(detailProduct);
        DetailProduct valueGet2 = detailProductGet.get();
        System.out.println("Value Get 2: " + valueGet2);

//        MAP
        Optional<String> optionalValueMap = Optional.ofNullable("Hello");
        Optional<Integer> length = optionalValueMap.map(String::length);
        System.out.println("Length: "+length.orElse(0));

//        MAP OBJECT 1
        Optional<DetailProduct> detailProductMap = Optional.of(detailProduct);
        Optional<String> description = detailProductMap.map(DetailProduct::getDescription);
        System.out.println("Description: "+description.orElse("No description available"));

//        MAP OBJECT 2
        Optional<DetailProduct> detailProductMap2 = Optional.of(detailProduct);
        Optional<Optional<Category>> categoryMap = detailProductMap2.map(DetailProduct::getCategory);
        System.out.println("Category: "+categoryMap);


        Optional<Integer> categoryProduct6 = detailProduct1.flatMap(DetailProduct::getCategory).map(Category::getName).map(String::length);
        System.out.println("Value categoryProduct6 length="+categoryProduct6);
    }

}


